<footer class="black-bg musica-footer">
        <div class="container">
            <div class="footer-logo">
                <a href="{{url('/')}}"><img src="images/home/qlogo-white.png" alt="quindara logo" title="QuinDara Events"></a>
            </div>
            <div class="footer-menu">
                <ul>
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/wcu')}}">Why Choose Us</a></li>
                    <li><a href="{{url('/event')}}">QuinDara Events</a></li>
                    <li><a href="{{url('/event')}}-detail">QuinDara Communication</a></li>
                    <li><a href="{{url('/event')}}">Gallery</a></li>
                    <li><a href="{{url('/contact')}}">Contact</a></li>
                </ul>
            </div>
            <div class="social-icon">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-soundcloud"></i></a></li>
                    <li><a href="#"><i class="fa fa-lastfm"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
            <br>
            <footer>&copy; {{date('Y')}} ASC. All rights reserved by Anant Soft Computing. </footer>
        </div>
    </footer>
