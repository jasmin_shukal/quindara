 <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ================= Favicon ================== -->
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="icon" sizes="72x72" href="images/favicon/android-icon-72x72.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <!-- Bootstrap css-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Ionicon css-->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Font Awesome css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- REVOLUTION SLIDER STYLES -->
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/layers.css">
    <link rel="stylesheet" type="text/css" href="css/navigation.css">
    <!-- Jquery Countdown css-->
    <link rel="stylesheet" href="css/jquery.countdown.css">
    <!-- Magnific popup css-->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- SLimple Light box-->
    <link rel="stylesheet" href="css/simplelightbox.css">
    <!-- Style css-->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive css-->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Modernizr js-->
    <script src="js/modernizr-2.8.3.min.js"></script>