<!-- == jQuery Librery == -->
    <script src="js/jquery-2.2.4.min.js"></script>
	<!-- == Bootsrap js File == -->
    <script src="js/bootstrap.min.js"></script>
    <!-- == Jquery Countdown == -->
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <!-- == Magnific Popup == -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <!-- == nicescroll == -->
    <script src="js/jquery.nicescroll.min.js"></script>
    <!-- == Jquery Player == -->
    <script src="js/jquery.jplayer.min.js"></script>
    <script src="js/jplayer.playlist.min.js"></script>
    <script type="text/javascript" src="js/audio-playlist.js"></script>
    <!-- == Light Box == -->
    <script src="js/simple-lightbox.min.js"></script>
    <!-- == Isotope == -->
    <script src="js/isotope.pkgd.min.js"></script>
    <!-- == Revolution Slider JS == -->
    <script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="js/revolution/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="js/revolution/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/revolution-active.js"></script>
	<!-- == Custom Js == -->
    <script src="js/custom.js"></script>
