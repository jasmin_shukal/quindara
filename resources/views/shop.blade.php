@extends('layouts.app')

@section('content')
<section class="banner-area shop-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1><strong>Shop</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->
<div class="main-content pdt40 pdb90">
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop1.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">AC/DC</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop2.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Slayer</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                    <span class="sale-button base-gradient-bg">SALE</span>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop3.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Slayer two</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop4.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Megadeth</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop5.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Slayer New</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop6.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Megadeth</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop7.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Megadeth</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop8.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Megadeth</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="shop-item">
                    <div class="image">
                        <img src="images/shop/shop9.png" alt="shop" class="img-responsive">
                    </div>
                    <div class="text">
                        <span class="item-name">Megadeth</span>
                        <a href="#" class="add-to-cart">+Add to cart</a>
                        <div class="price">$100</div>
                    </div>
                </div><!--/.shop-item-->
            </div>
        </div>
        <div class="musica-navigation-pagination">
            <div class="musica-navigation">
                <a href="#"><i class="fa fa-angle-left"></i></a>
                <a href="#"><i class="fa fa-angle-right"></i></a>
            </div>
            <div class="musica-pagination">
                <ul>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
