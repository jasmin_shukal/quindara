 <header class="musica-header home-page-header">
        <div class="menu-area desktop-menu">
            <div class="header-menu">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <a href="{{url('/')}}" class="logo-brand">
                                <img src="images/home/quindara.png" class="img-responsive" alt="logo">
                            </a>
                        </div>
                        <!-- <span class="shop-icon">
                            <a href="cart.html"><i class="ion-bag"></i></a>
                            <span class="shop-count base-gradient-bg">1 </span>
                        </span> -->
                        <nav id="easy-menu">
                            <div class="button">
                                <i class="fa fa-bars"></i>
                            </div>
                            <ul class="menu-list">
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li><a href="{{url('/wcu')}}">Why Choose Us</a></li>
                                <li><a href="{{url('/event')}}">QuinDara Events</a></li>
                                <li><a href="{{url('/event')}}-detail">QuinDara Communication</a></li>
                                <!-- <li><a href="{{url('/event')}}">Events</a>
                                    <ul class="dropdown">
                                        <li><a href="{{url('/event')}}">QuinDara Events</a></li>
                                        <li><a href="{{url('/event')}}-detail">QuinDara Communication</a></li>
                                    </ul>
                                </li> -->
								<li><a href="{{url('/gallery')}}">Gallery</a></li>
                                <li><a href="{{url('/contact')}}">Contact Us</a></li>
                           </ul>
                       </nav>
                       <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div><!--/.menu-area-->
        <div class="mobile-menu">
            <div class="container">
                <div class="mobile-logo-search-humbarger">
                    <div class="logo">
                        <a href="{{url('/')}}" class="logo-brand">
                            <img src="images/home/quindara.png" class="img-responsive" alt="logo">
                        </a>
                    </div>
                    <div class="humbarger-button">
                        <i class="fa fa-bars"></i>
                    </div>
                </div>
            </div>
        </div><!--/.menu-area-->
        <nav class="mobile-background-nav">
            <div class="mobile-inner">
                <span class="mobile-menu-close"><i class="fa fa-times"></i></span>
                <ul class="menu-accordion">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/wcu')}}">Why Choose Us</a></li>
                    <li><a href="{{url('/event')}}">QuinDara Events</a></li>
                    <li><a href="{{url('/event')}}-detail">QuinDara Communication</a></li>
                    <li><a href="{{url('/event')}}">Gallery</a></li>
                    <li><a href="{{url('/contact')}}">Contact</a></li>

                    <!-- <li><a href="javascript:void(0);" class="has-submenu">Event<i class="fa fa-angle-down pull-right"></i></a>
                        <ul class="menu-inner">
                            <li><a href="event-list.html">QuinDara Events</a></li>
                            <li><a href="event-details.html">QuinDara Communication</a></li>
                        </ul>
                    </li> -->
                    <!-- <li><a href="javascript:void(0);" class="has-submenu">album<i class="fa fa-angle-down pull-right"></i></a>
                        <ul class="menu-inner">
                            <li><a href="album.html">Gallery</a></li>
                            <li><a href="album-detail.html">album Detail</a></li>
                        </ul>
                    </li> -->
                    <!-- <li><a href="javascript:void(0);" class="has-submenu">news<i class="fa fa-angle-down pull-right"></i></a>
                        <ul class="menu-inner">
                            <li><a href="blog.html">news</a></li>
                            <li><a href="blog-detail.html">news Right Sidebar</a></li>
                            <li><a href="blog-detail-left-sidebar.html">news left Sidebar</a></li>
                            <li><a href="blog-detail-fullwidth.html">news Full width</a></li>
                            <li><a href="blog-detail-audio-post.html">news audio post</a></li>
                            <li><a href="blog-detail-gallery-post.html">news gallery post</a></li>
                            <li><a href="blog-detail-map-post.html">news Map post</a></li>
                            <li><a href="blog-detail-qoute-post.html">news qoute post</a></li>
                            <li><a href="blog-detail-video-post.html">news Video post</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="has-submenu">shop<i class="fa fa-angle-down pull-right"></i></a>
                        <ul class="menu-inner">
                            <li><a href="shop.html">shop</a></li>
                            <li><a href="shop-detail.html">shop Detail</a></li>
                            <li><a href="cart.html">cart</a></li>
                        </ul>
                    </li> -->

               </ul>
            </div>
       </nav>
    </header>
