<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>QuinDara</title>
    @include('layouts.inc.header')
</head>
<body>
    <div class="preloader">
        <div class="loader">
            <div class="loader-inner line-scale-pulse-out">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
    </div>
    @include('layouts.inc.top-header')

            @yield('content')

    @include('layouts.inc.footer')
    @include('layouts.inc.js')
</body>
</html>
