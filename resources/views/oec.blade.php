@extends('layouts.app')

@section('content')

<section class="banner-area blog-banner oec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1>Overseas Education Center</h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->
        <!-- my -->
    <div class="main-content pdt40 pdb110">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sidebar">
                        <aside class="sidebar-widget">
                            <div class="widget-heading">
                                <h3>About <strong> Overseas Education Center</strong></h3>
                            </div>
                             <p class="abt">Overseas Education Center (OEC) was established with the core objective of helping students in achieving their dreams of overseas education. Since conception in 2003, we have worked to develop our expertise for overseas education and visa - formalities. Over the years we have also created a robust network with leading institutions worldwide that gives us the ability to provide better education services to our students.
                                    </p>
                        </aside><!--/.sidebar-widget-->
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- commitment -->
                    <div class="comment-form">
                        <h3 class="heading">Enquiry Now</h3>
                        <form id="contact">
                            <div class="row pdt20">
                                <div class="single-input">
                                    <input type="text" name="name" placeholder="Name" class="form-control" required="">
                                </div>
                                <div class="single-input">
                                    <input type="email" name="email" placeholder="Email" class="form-control" required="">
                                </div>
                                 <div class="single-input">
                                    <input type="text" name="number" placeholder="Phone Number" class="form-control" required="">
                                </div> <div class="single-input">
                                    <input type="text" name="hightest-qualification" placeholder="Hightest Qualification" class="form-control" required="">
                                </div>
                                <div class="single-input">
                                    <input type="text" name="course" placeholder="Course Interested In" class="form-control" required="">
                                </div>
                            </div>
                            <div class="single-input">
                                <textarea name="message" rows="8" cols="80" placeholder="Enquiry" required=""></textarea>
                            </div>
                            <!-- Replace the 'value' of following input box with the
                            Email address where you want to get emails via contact form -->
                            <input type="hidden" name="recipient_email" value="contact@enroutedigitallab.com">
                            <!-- Replace the 'value' of following input box with the sender
                            email address, note that: email address should from existing domain  -->
                            <input type="hidden" name="from_email" value="contact@enroutedigitallab.com">
                            <center>
                                <button type="submit" class="musica-button">Send</button>
                            </center>
                        </form>
                    </div><!--/.comment-form-->
                </div>
            </div>
        </div>
    </div>



<!-- about Us -->
<!-- <section class="">
        <div class="main-content pdt40 pdb110">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget myabt">
                                <div class="section-heading text-center">
                                    <h2>About <strong> Overseas Education Center</strong></h2>
                                </div>
                                    <p class="abt">Overseas Education Center (OEC) was established with the core objective of helping students in achieving their dreams of overseas education. Since conception in 2003, we have worked to develop our expertise for overseas education and visa - formalities. Over the years we have also created a robust network with leading institutions worldwide that gives us the ability to provide better education services to our students.
                                    </p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
<!-- end of about Us -->


@endsection
