@extends('layouts.app')

@section('content')
<section class="banner-area blog-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1>Why Choose <strong>Us</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->

    <!-- about Us -->
    <section class="">
        <div class="main-content pdt110 pdb110 cd-fixed-bg cd-bg-abt">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget">
                                <div class="section-heading text-center">
                                    <h2><strong>About Us</strong></h2>
                                </div>
                                    <p class="abt">Founded in 2019, QuinDara Events was started with a purpose to serve our clients the best in the industry. With clients requirements and our vision we make sure each event is successful, fun as well as something that our clients would remember forever. We strive to be the most reliable and creative event Management Company in India. We make sure you get the best of service at the most cost effective rate.</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end of about Us -->
    <!-- add  -->
    <!-- commitment -->
    <section class="gallery-section pdb85 black-bg">
         <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget myabt">
                                <div class="section-heading text-center">
                                    <h2>Commitment</h2>
                                </div>
                                    <p class="abt">QuinDara Events is a full service, lifestyle based, professional event planning company that specializes in corporate events, dinner galas, fundraisers, long service awards, grand openings, conferences and private events. We love details! And our commitment is to see each event through from start to finish while keeping the goals, vision, budget and client's needs in mind at all times. We are committed to following through on every detail so you don't have to. From a 1000 person dinner gala to a small intimate private party or meeting, QuinDara Events has experienced it all.</p>
                                    <p class="abt">Conferences and events in India are ideal for corporate, incentive and social groups. This is because magical India offers the best of both worlds the culture and tradition of India as well as the sophistication of first world hotels, transport and communication structures, top class venues, corporate conference planning organisations and support services. Clients enjoy the privilege of the variety of options our country offers in the selection of venues and facilities when hosting a function. Multinational companies and the world at large recognise the convenience, beauty and wonders of hosting events in India.</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- end of commitment -->
    <!-- Service -->
    <section class="">
        <div class="main-content pdt110 pdb110 cd-fixed-bg cd-bg-abt">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget">
                                <div class="section-heading text-center">
                                    <h2><strong>We Are</strong></h2>
                                </div>
                                    <p class="abt">QuinDara Events welcomes local and foreign clients to share the platform with them in planning and hosting their next event. We pride ourselves in working within the small-to-medium size sector preferring the personal touch inherent to this group. Should you be considering holding a conference or event, please contact us. We would be delighted to assist you in gathering information to prepare a bid document as part of your conference or event planning. QuinDara Events can assist with the planning and implementation of a range of corporate functions and social events. At QuinDara Events we believe a successful event is directly related to the guest experience.</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- end of about Us -->

    <!-- our Team -->
    <section class="news-section section-padding">
        <div class="container">
            <div class="section-heading text-center">
                 <h1>Our <strong>Team</strong></h1>
            </div>
            <div class="col-lg-12">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="{{asset('images/services/rohit.png')}}" alt="Team" class="img-responsive" style=" margin-left: 133px;">
                        <div class="base-gradient-bg">
                            <span>Director, CEO, </span>QuinDara Events
                        </div>
                        <h3><a href="#">Rohit Sharma</a></h3>

                    </div><!--/.blog-post-->
                    <p class="abt">Owner and lead planner, Rohit Sharma, is passionate about lifestyle, urban living and executing exciting events for his clients. Born and raised in Mumbai, India has been planning corporate events and makes sure that they are a hit !!</p>
                    <p class="divyang-read-more-content1 abt" style="display: none;">Rohit studied Marketing with Animation & VFX at the University of Mumbai where his passion for design grew and he was exposed to the event planning industry for the first time. Designs, Planning, Visualisation were his favourites. From there he worked full time with some of the leading event planning companies, where he was fortunate to be involved with and lead planner on many incredible projects locally, Nationally and Internationally. Rohit also has experience working in the International Higher Education sector and hence has the expertise to handle corporate clients. Some events Rohit has managed and executed since opening QuinDara Events are: University Conferences, Seminars/ Webinars, Launch Events, Weddings, Private gatherings and celebrations.</p>
                    <button data-btn-id="1" class="divyang-read-more-button musica-button">Read More</button>
                </div>
                <div class="col-md-6">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="{{asset('images/services/subhi.png')}}" alt="news" class="img-responsive" style=" margin-left: 133px;">
                        <div class="base-gradient-bg">
                            <span>Celebrity, Talent &</span> Public Relations Manager
                        </div>
                        <h3><a href="#">Surbhi Tiwari</a></h3>
                    </div><!--/.blog-post-->
                    <p class="abt">Surbhi manages public relations and partners at QuinDara Events. Surbhi leads QuinDara team, where she manages a talented team to drive results for clients across all out events. Surbhi started in QuinDara Events in 2019.</p>
                    <p class="abt divyang-read-more-content2" style="display: none;">Since her time at QuinDara Events, she has worked on some major clients plus many others in the space. Surbhi’s client work has been recognised by the clients and have received many applauds for the same. Surbhi graduated with Bachelor of Computer Science and then moved towards to her interest in Public relations & Marketing.</p>
                    <button data-btn-id="2" class="divyang-read-more-button musica-button">Read More</button>
                </div>
               </div>
            </div>
           <!--  <div class="text-center pdt40 pdb25">
                <a href="#" class="musica-button">SEE ALL</a>
            </div> -->
        </div>
    </section>

@endsection
