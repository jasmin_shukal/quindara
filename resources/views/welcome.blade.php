@extends('layouts.app')

@section('content')
<style>
.name
{
    background-color: red;
}
</style>

    <section class="slider-section">
        <div class="slider-ovelay"></div>
        <div id="rev_slider_1052_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="web-product-dark122" data-source="gallery">
            <!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
            	<div id="rev_slider_1052_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">
            <ul>	<!-- SLIDE  -->
                    @foreach ($all_header_slider as $slider)
                    <li data-index="rs-{{$slider->id}}" data-transition="fade" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1500"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" >
                        <!-- MAIN IMAGE -->
                        @php
                        $slider_bg_img = get_attachment_image_by_id($slider->image,null,false);
                        @endphp
                        <img src="{{$slider_bg_img['img_url']}}" alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS 1-->
                        <div class="tp-caption tp-resizeme"
                             id="slide-{{$slider->id}}-layer-7"
                            data-x="['middle','middle','middle','middle']"
                            data-hoffset="['-425','-425',0,'0']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['-170','-170','-170','-170']"
                            data-fontsize="['75','75','60','60']"
                            data-lineheight="['75','75','60','60']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            >{{$slider->title}}</div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption slider-blod-text tp-resizeme"
                            id="slide-2946-layer-6"
                            data-x="['middle','middle','middle','middle']"
                            data-hoffset="['-280','-280',0,'0']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['-70','-70','-70','-70']"
                            data-fontsize="['130','130','80','60']"
                            data-lineheight="['130','130','80','60']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1250,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">
                            <span class="text-gradient-color">{{$slider->description}}</span></div>
                        <!-- LAYER NR. 3 -->
                       <!--  <div class="tp-caption tp-resizeme"
                            id="slide-2946-layer-5"
                            data-x="['middle','middle','middle','middle']"
                            data-hoffset="['-337','-337',0,'0']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['50','50','20','20']"
                            data-fontsize="['75','75','60','60']"
                            data-lineheight="['75','75','60','60']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1500,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]">in the concert </div> -->
                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption  tp-resizeme slider-arrow-button"
                            id="slide-2946-layer-4"
                            data-x="['middle','middle','middle','middle']"
                            data-hoffset="['-547','-547',0,'0']"
                            data-y="['middle','middle','middle','middle']"
                            data-voffset="['160','160','100','100']"
                            data-width="['48','48','150','150']"
                            data-height="['48','48','48','48']"
                            data-whitespace="nowrap"
                            data-type="text"
                            data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-{{$slider->id}}","delay":""}]'
                            data-responsive_offset="on"
                            data-responsive="off"
                            data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1750,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(51, 51, 51, 1.00);bg:rgba(255, 255, 255, 1.00);bw:2px 2px 2px 2px;"}]'
                            data-textAlign="['left','left','left','left']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]" >
                                <?php /* <a href="{{url('/event')}}" class="musica-button-two">Discover</a> */ ?>
                        </div>
                    </li>
                    @endforeach

            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
        </div><!-- END REVOLUTION SLIDER -->
    </section>

    <?php /* <section class="gallery-section black-bg">
        <div class="main-content pdt40 pdb110">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget myabt">
                                <div class="section-heading text-center">
                                    <h2>Who <strong>We Are</strong></h2>
                                </div>
                                <p class="abt">QuinDara Events is a full service, lifestyle based, professional event planning company that specializes in corporate events, dinner galas, fundraisers, long service awards, grand openings, conferences and private events.
                                We love details! And our commitment is to see each event through from start to finish while keeping the goals, vision, budget and client's needs in mind at all times. We are committed to following through on every detail so you don't have to. From a 1000 person dinner gala to a small intimate private party or meeting, QuinDara Events has experienced it all.</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center pdt40">
                <a href="{{url('/wcu')}}" class="musica-button">CHECK ALL</a>
            </div>
        </div>
    </section>*/ ?>
    <section class="who-we-are-section">
        <div class="divyang-about-bg">
            <div class="container-fluid">
                <div class="row">
                    <div class="bg-black col-xs-12 col-sm-12 col-md-3">
                        <div class="section-title ptb-p ml-p divyang-text-upper">
                            <h1>who<br/><span class="text-gradient-color">we are</span></h1>
                            <p>about us</p>
                            <hr>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 p-0">
                        <div class="bg-dark-grey section-content col-xs-12 col-sm-12 col-md-6 p-25">
                            <h4 class="text-important" style="text-align: justify;">{!! get_static_option('admin_who_we_are') !!}</h4>
                            {{-- <h4 class="text-important pt-10" style="text-align: justify;">QuinDara communications is a new age digital content company that brings together content, technology and data delivery with high quality creatives and sales solutions. We love details! And our commitment is to see each event or planning need go through start to finish while keeping the goals, vision, budget and client's needs in mind at all times. We are committed to following through on every detail so you don't have to. From a 1000 person dinner gala to company profiling and branding, QuinDara has experienced it all.</h4> --}}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="event-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-md-6 my">
                        <div class="box">
                           <a href="{{url('/event')}}">
                                <img src="images/home/qevent.png" class="img-responsive" alt="events" title="Quindara Events">
                            </a>
                        </div>
                        <!-- <div class="section-heading">
                            <h2>QuinDara <strong>Events</strong></h2>
                            <div class="col-md-6">
                                <a href="#">
                                    <img src="images/home/qevent.png" class="img-responsive" alt="album">
                                </a>
                            </div>
                        </div> -->
                    </div>

                    <div class="col-md-6">
                        <div class="box">
                           <a href="{{url('/event')}}-detail">
                                <img src="images/home/qcommunication.png" class="img-responsive" alt="events" title="Quindara Communication">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="section-heading">
                        <h2>Quindara <strong>Events</strong> & <strong>Communication</strong></h2>
                    </div> -->
                </div>
            </div>
           <!--  <div class="pdb80">
                <div class="upcoming-event">
                    <div class="fornt-part">
                        <div class="date">26 March, 17</div>
                        <div class="singer">Rock festival</div>
                        <div class="venue">Melbourne, Australia</div>
                        <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                    </div>
                    <div class="back-part">
                        <div class="event-counter">
                            <div id="event-counter-one" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                        </div>
                    </div>
                </div>
                <div class="upcoming-event">
                    <div class="fornt-part">
                        <div class="date">10 April, 17</div>
                        <div class="singer">Metal mania</div>
                        <div class="venue">Melbourne, Australia</div>
                        <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                    </div>
                    <div class="back-part">
                        <div class="event-counter">
                            <div id="event-counter-two" class="musica-counter-active" data-value-year="2017" data-value-month="4" data-value-day="10" data-value-zone="+10" ></div>
                        </div>
                    </div>
                </div>
                <div class="upcoming-event">
                    <div class="fornt-part">
                        <div class="date">26 March, 17</div>
                        <div class="singer">Rock festival</div>
                        <div class="venue">Melbourne, Australia</div>
                        <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                    </div>
                    <div class="back-part">
                        <div class="event-counter">
                            <div id="event-counter-three" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="#" class="musica-button-two base-gradient-bg">CHECK ALL</a>
            </div> -->
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">

            <div class="section-heading text-center">
                <h1>Our <strong>Team</strong></h1>
            </div>
            <div class="col-lg-12">
            <div class="row row-eq-height">
                <div class="col-md-6">
                    <div class="sidebar">
                        <aside class="sidebar-widget">
                            <div class="blog-post">
                                <img src="/images/services/rohit.png" alt="Team" class="">
                            <div class="name">
                                <h3 style="background: #272732;">Rohit Sharma</h3>
                            </div>
                            <div class="postin" style="font-size: large;">
                                <span>Director, CEO, </span>QuinDara Events
                            </div>
                            </div><!--/.blog-post-->
                        </aside>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="sidebar">
                        <aside class="sidebar-widget">
                            <div class="blog-post">
                                <img src="/images/services/subhi.png" alt="news" style="">
                                <h3 style="background: #272732;">Surbhi Tiwari</h3>
                                <div class="postin mt-3" style="font-size: large;">
                                    <span>Celebrity, Talent &</span> Public Relations Manager
                                </div>
                            </div><!--/.blog-post-->
                        </aside>
                    </div>
                </div>
               </div>
            </div>
            <div class="text-center pdt40 pdb25">
                <a href="{{url('wcu')}}" class="musica-button">View More</a>
            </div>
        </div>
    </section>

    {{-- <section class="album-section section-padding">
        <div class="container">
                <div class="section-heading text-center">
                   <h2>Our <strong>Products</strong> & <strong> Services</strong></h2>
                </div>
            <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Conference & Corporate </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                        <h3 class="heading myservice">Conference & Corporate Events</h3>
                    </div><!--/.album-item-->

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/3.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Product Launch</h3>
                            <span class="sub-heading">Events</span>
                        </a><h3 class="heading myservice">Product Launch</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/3.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Music Video Launch</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                        <h3 class="heading myservice">Music Video Launch</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Weddings & Ring Ceremonies</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Weddings & Ring Ceremonies</h3>
                    </div><!--/.album-item-->
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Live Events & Concerts</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Live Events & Concerts</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/6.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Charity & Non-Profitable </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Charity & Non-Profitable Events</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/2.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Gala Dinners & Award Ceremonies</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Gala Dinners & Award Ceremonies</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/2.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Webinars / Seminars / Educational </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Webinars / Seminars / Educational Events</h3>
                    </div><!--/.album-item-->
                </div>
            </div>
            <div class="text-center pdt40">
                <a href="{{url('/event')}}" class="musica-button">View More</a>
            </div>
        </div>
    </section> --}}

    <section class="album-section section-padding">
        <div class="container">
            <div class="section-heading text-center">
               <h2>Our <strong>Products</strong> & <strong> Services</strong></h2>
            </div>
                @foreach($all_service as $data)
                @if ($loop->first)
                <div class="row row-eq-height">
                @elseif($loop->index%4 == 0)
                    </div>
                    <div class="row row-eq-height">
                @endif
                @php
                    $header_bg_img = get_attachment_image_by_id($data->image,null,false);
                @endphp
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="{{$header_bg_img['img_url'] }}" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Events</h3>
                            <span class="sub-heading">Service</span>
                        </a>
                        <h3 class="heading myservice">{{$data->title}}</h3>
                    </div><!--/.album-item-->
                </div>
                @endforeach
            </div>

            <div class="text-center pdt40">
                <a href="{{url('/event')}}" class="musica-button">View More</a>
            </div>
        </div>
    </section>

    <section class="gallery-section black-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-heading no-padding">
                        <h2>Our <strong>Gallery</strong></h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gallery-filter">
                        <ul>
                            <li><a href="#!" data-filter="*">ALL</a></li>
                            <li><a href="#!" data-filter=".concert">Seminars</a></li>
                            <li><a href="#!" data-filter=".poster">Weddings</a></li>
                            <li><a href="#!" data-filter=".painting">Concerts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="gallery-grid">
                <div class="grid-item painting poster">
                    <img src="images/home/n7.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item grid-another-width concert">
                    <img src="images/event/event2.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event2.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item concert">
                    <img src="images/home/n10.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n10.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item painting concert">
                    <img src="images/home/n9.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n9.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>

                <div class="grid-item poster concert">
                    <img src="images/event/event5.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event5.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item  painting poster">
                    <img src="images/event/event6.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event6.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item poster">
                    <img src="images/event/event7.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
            </div>
        </div>
    </section>

    <?php /*
    <section class="gallery-section black-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-heading no-padding">
                        <h2>Photo <strong>Gallery</strong></h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gallery-filter">
                        <ul>
                            <li><a href="#!" data-filter="*">ALL</a></li>
                            <li><a href="#!" data-filter=".concert">concert</a></li>
                            <li><a href="#!" data-filter=".poster">poster</a></li>
                            <li><a href="#!" data-filter=".painting">painting</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="gallery-grid">
                <div class="grid-item painting poster">
                    <img src="images/event/1.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/01.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item grid-another-width concert">
                    <img src="images/home/gallery1.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery1.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item concert">
                    <img src="images/home/gallery3.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery3.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item painting concert">
                    <img src="images/home/gallery4.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery4.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>

                <div class="grid-item poster concert">
                    <img src="images/home/gallery5.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery5.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item  painting poster">
                    <img src="images/home/gallery6.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery6.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item poster">
                    <img src="images/home/gallery7.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/gallery7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="video-box-section text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="video-text">
                        <div class="section-heading">
                            <h2><span>Our Event</span> <strong class="text-gradient-color">video</strong></h2>
                        </div>
                        <div class="video-play-icon">
                            <a href="#">
                                <span><i class="ion-ios-play"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="section section-padding artist-section">
            <div class="container">
                 <div class="section-heading text-center">
               <h1>Our <strong>Team</strong></h1>
            </div>
                <div class="row artists">
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/1.jpg" alt="...">
                                </a>
                            <h4 class="artist-name"><a href="artist-single.html">Smith Bravo</a></h4>
                             <p class="artist-role">Rock Singer</p> -->
                        <!-- </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/2.jpg" alt="...">
                                </a>
                            <h4 class="artist-name"><a href="artist-single.html">Maria Jack</a></h4> -->
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        <!-- </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/3.jpg" alt="...">
                                </a>
                            <h4 class="artist-name"><a href="artist-single.html">Merry Luke</a></h4> -->
                           <!--  <p class="artist-role">Rock Singer</p> -->
                      <!--   </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/4.jpg" alt="...">
                                </a>
                            <h4 class="artist-name"><a href="artist-single.html">Luther Gulik</a></h4> -->
                           <!--  <p class="artist-role">Rock Singer</p> -->
                       <!--  </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/5.jpg" alt="...">
                                </a>
                            <h4 class="artist-name"><a href="artist-single.html">Bond J.Kerry</a></h4> -->
                           <!--  <p class="artist-role">Rock Singer</p> -->
                       <!--  </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="http://appscred.com/html/mxtonz/images/artist/6.jpg" alt="..."> -->
                                <!-- </a>
                            <h4 class="artist-name"><a href="artist-single.html">Olivia Keth</a></h4> -->
                           <!--  <p class="artist-role">Rock Singer</p> -->
                       <!--  </div>
                    </div>
                </div>
            </div>
    </div>

    <section class="gallery-section black-bg">
        <div class="main-content pdt40 pdb110">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget myabt">
                                <div class="section-heading text-center">
                                    <h2>Who <strong>We Are</strong></h2>
                                </div>
                                <p class="abt">QuinDara Events is a full service, lifestyle based, professional event planning company that specializes in corporate events, dinner galas, fundraisers, long service awards, grand openings, conferences and private events.
                                We love details! And our commitment is to see each event through from start to finish while keeping the goals, vision, budget and client's needs in mind at all times. We are committed to following through on every detail so you don't have to. From a 1000 person dinner gala to a small intimate private party or meeting, QuinDara Events has experienced it all.</p>

                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    */ ?>

    <section>
        <div class="container">
            <!-- commitment -->
            <div class="comment-form">
                <div class="section-heading text-center">
                    <h1>Get In <strong>Touch</strong></h1>
                </div>
                    <!-- <h3 class="heading">Enquiry Now</h3> -->
                <form id="contact">
                    <div class="row pdt20">
                        <div class="col-md-4 col-xs-12 single-input">
                            <input type="text" name="name" placeholder="Name" class="form-control" required="">
                        </div>
                        <div class="col-md-4 col-xs-12 single-input">
                            <input type="email" name="email" placeholder="Email" class="form-control" required="">
                        </div>
                        <div class="col-md-4 col-xs-12 single-input">
                            <input type="text" name="number" placeholder="Number" class="form-control" required="">
                        </div>
                        <!-- <div class="single-input">
                            <input type="text" name="hightest-qualification" placeholder="Hightest Qualification" class="form-control" required="">
                        </div>  -->
                        <!-- <div class="single-input">
                            <input type="text" name="course" placeholder="Course Interested In" class="form-control" required="">
                        </div>  -->
                        <div class="col-md-12 single-input">
                            <textarea name="message" rows="8" cols="80" placeholder="Comments" required=""></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="recipient_email" value="contact@enroutedigitallab.com">
                    <input type="hidden" name="from_email" value="contact@enroutedigitallab.com">
                    <center><button type="submit" class="musica-button">Send</button></center>
                </form>
            </div>
        </div>
    </section>
    <?php /*
    <section class="news-section section-padding">
        <div class="container">
            <div class="section-heading text-center">
                <h2>Recent <strong>News</strong></h2>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news1.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Metal god in Miami</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news2.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Musical tour 2017</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news3.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Big rock concert</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news3.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Big rock concert</a></h3>
                    </div><!--/.blog-post-->
                </div>
            </div>
             <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news1.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Metal god in Miami</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news2.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Musical tour 2017</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news3.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Big rock concert</a></h3>
                    </div><!--/.blog-post-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="blog-post">
                        <div class="gradent-overlay"></div>
                        <img src="images/home/news3.jpg" alt="news" class="img-responsive">
                        <div class="post-date base-gradient-bg">
                            <span>30</span>may
                        </div>
                        <h3><a href="#">Big rock concert</a></h3>
                    </div><!--/.blog-post-->
                </div>
            </div>
            <div class="text-center pdt40 pdb25">
                <a href="#" class="musica-button">SEE ALL</a>
            </div>
        </div>
    </section>
    */ ?>
@endsection
