
@extends('layouts.app')

@section('content')
<section class="banner-area blog-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1> <strong>Gallery</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="gallery-grid" style="position: relative; height: 599.549px;">
                <div class="grid-item painting poster" style="position: absolute; left: 0%; top: 0px;">
                    <img src="images/home/n7.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item grid-another-width concert" style="position: absolute; left: 19.9481%; top: 0px;">
                    <img src="images/event/event2.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event2.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item concert" style="position: absolute; left: 59.9777%; top: 0px;">
                    <img src="images/home/n10.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n10.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item painting concert" style="position: absolute; left: 79.9925%; top: 0px;">
                    <img src="images/home/n9.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/home/n9.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>

                <div class="grid-item poster concert" style="position: absolute; left: 0%; top: 199px;">
                    <img src="images/event/event5.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event5.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item  painting poster" style="position: absolute; left: 59.9777%; top: 199px;">
                    <img src="images/event/event6.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event6.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item poster" style="position: absolute; left: 79.9925%; top: 199px;">
                    <img src="images/event/event7.jpg" class="img-responsive" alt="portfolio image">
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
            </div>
            @endsection