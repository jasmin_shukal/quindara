@extends('layouts.app')

@section('content')
<style>
    .cd-bg-1
    {
        background-image: url("http://quindara.in/images/home/n1.JPG");
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-position: center center;
        min-height: 100%;
        background-size: cover;
        -webkit-background-size: cover !important;
        -moz-background-size: cover !important;
        -o-background-size: cover;
    }
</style>
<section class="banner-area blog-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1>Contact <strong>Us</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->
<section>
    <!-- my -->
    <div class="main-content pdt40 pdb110 cd-fixed-bg cd-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="sidebar">
                        <aside class="sidebar-widget" style="min-height: 24em; max-height: 24em; height: 24em;">
                            <div class="widget-heading">
                                <h3>Address</h3>
                            </div>
                             <p class="abt">
                                Sonali Bunglaow, Lions hall lane,<br>  Race Course Circle,<br>  Vadodara-390007, Gujarat, India.
                            </p>
                        </aside><!--/.sidebar-widget-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar">
                        <aside class="sidebar-widget" style="min-height: 24em; max-height: 24em; height: 24em;">
                            <div class="widget-heading">
                                <h3>Contact Info</h3>
                            </div>
                           <p class="abt">
                            <a aria-label="Contact Number" href="tel:+91 9879737199" target="_blank" class="hoverline">+91 98797 37199</a>
                            <br>
                            <a aria-label="Contact Number" href="tel:+91 9167910505" target="_blank" class="hoverline">+91 91679 10505</a>

                            </p>
                        </aside><!--/.sidebar-widget-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar">
                        <aside class="sidebar-widget" style="min-height: 24em; max-height: 24em; height: 24em;">
                            <div class="widget-heading">
                                <h3>Email</h3>
                            </div>
                           <p class="abt">
                              enquiry@quindara.in
                            </p>
                            <p class="abt">
                            marketing@quindara.in
                            </p>
                        </aside><!--/.sidebar-widget-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- contactr -->
      <div class="main-content pdb110">
        <div class="container">
            <div class="row">


    <!-- commitment -->
    <div class="comment-form">
         <center><h3 class="heading">Conact Now</h3></center>
        <form id="contact">
            <div class="row pdt20">
                <div class="single-input">
                    <input type="text" name="name" placeholder="Name" class="form-control" required="">
                </div>
                <div class="single-input">
                    <input type="email" name="email" placeholder="Email" class="form-control" required="">
                </div>
            </div>
            <div class="single-input">
                <textarea name="message" rows="8" cols="80" placeholder="Comments" required=""></textarea>
            </div>
            <!-- Replace the 'value' of following input box with the
            Email address where you want to get emails via contact form -->
            <input type="hidden" name="recipient_email" value="contact@enroutedigitallab.com">
            <!-- Replace the 'value' of following input box with the sender
            email address, note that: email address should from existing domain  -->
            <input type="hidden" name="from_email" value="contact@enroutedigitallab.com">
           <center> <button type="submit" class="musica-button">Send</button></center>
        </form>
    </div><!--/.comment-form-->
    <!-- end of commitment -->
</div></div></div>




@endsection

