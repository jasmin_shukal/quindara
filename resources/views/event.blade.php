@extends('layouts.app')

@section('content')


<section class="banner-area event-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1><strong>Welcome To QuinDara Events</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->

    <!-- mission aand Vision -->
    <section class="text-center core">
      <h2>Core Values</h2><br><br>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200" >
            <span class="fa fa-check"></span>
            <h3 class="icons">Vision</h3>
            <p class="lead">To be a market driven company that is a preferred service provider in the events management industry within its area of operation.</p>
          </div>
          <div class="col-lg-4 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200">
            <span class="fa fa-info"></span>
            <h3 class="icons">Mission </h3>
            <p class="lead">Integrated event execution with passion and excellence on time and within budget. </p>
          </div>
          <div class="clearfix visible-md-block visible-sm-block"></div>
          <div class="col-lg-4 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200">
            <span class="fa fa-file"></span>
            <h3 class="icons">Objectives</h3>
            <p class="lead">• To provide a full range of procurement and conference management services including value added services </p>
          </div>

        </div>

      </div>
    </section>
    <!-- end of mission - vision -->
    <!--services -->
    <!-- Our Products  -->
        <!-- Our Products  -->

    <section class="album-section section-padding">
        <div class="container">
            <div class="section-heading text-center">
               <h2>Our <strong>Products</strong> & <strong> Services</strong></h2>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Conference & Corporate </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                        <h3 class="heading myservice">Conference & Corporate Events</h3>
                    </div><!--/.album-item-->

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/3.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Product Launch</h3>
                            <span class="sub-heading">Events</span>
                        </a><h3 class="heading myservice">Product Launch</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/3.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Music Video Launch</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                        <h3 class="heading myservice">Music Video Launch</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Weddings & Ring Ceremonies</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Weddings & Ring Ceremonies</h3>
                    </div><!--/.album-item-->
                </div>
            </div>
            <div class="row row-eq-height">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/1.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Live Events & Concerts</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Live Events & Concerts</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/6.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Charity & Non-Profitable </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Charity & Non-Profitable Events</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/home/2.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Gala Dinners & Award Ceremonies</h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Gala Dinners & Award Ceremonies</h3>
                    </div><!--/.album-item-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="album-item">
                        <a href="#">
                            <img src="images/services/2.jpg" class="img-responsive" alt="album">
                            <div class="overlay base-gradient-bg"></div>
                            <h3 class="heading">Webinars / Seminars / Educational </h3>
                            <span class="sub-heading">Events</span>
                        </a>
                            <h3 class="heading myservice">Webinars / Seminars / Educational Events</h3>
                    </div><!--/.album-item-->
                </div>
            </div>
            <div class="text-center pdt40">
                <a href="{{url('/event')}}" class="musica-button">View More</a>
            </div>
        </div>
    </section>
    <!-- end of Products -->
    <!-- end of Products -->
     <!-- core Us  -->
    <section class="gallery-section black-bg">
        <div class="main-content pdt110 pdb110 cd-fixed-bg cd-bg-abt">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sidebar">
                            <aside class="sidebar-widget">
                                <div class="section-heading text-center">
                                    <h2>How <strong>we operate</strong></h2>
                                </div>
                                <p class="abt">We believe that we should act as conductor, bringing all the variety of services and suppliers to
                                order, ensuring that everyone is attuned, and simultaneously to act as a conduit between the
                                client and all sub-contractors involved with the conference or event. We offer a turnkey operation
                                for our clients, eliminating the bother of having to deal with numerous different suppliers and all
                                the intricate details involved in organizing an event.</p>

                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="text-center pdt40">
                <a href="{{url('/wcu')}}" class="musica-button">CHECK ALL</a>
            </div> -->
        </div>
    </section>
    <!-- end of abt us -->

<!-- Add Services -->
    <section class="album-section section-padding">
        <div class="container">
            <div class="row">
                <button class="accordion">Planning</button>

                <div class="panel">
                     <h4 class="abt white text-center">We Operate</h4><br>
                    <p class="abt white">We drive the complete planning process using the effective skills required for professional
                    services within the industry, whilst at the same time using the wealth of knowledge that
                    the client can contribute. The clients briefing directs all planning from the initiation to
                    closure.</p>
                <p class="abt white">
                    <ul class="listing">
                      <li>proactive</li>
                      <li>document all discussions</li>
                      <li>allocate responsibilities</li>
                       <li>follow up action dates, and</li>
                        <li>ensure that things get done on time and correctly</li>
                    </ul>
                </p>

                </div>

                <button class="accordion">Booking </button>
                <div class="panel">
                  <p class="abt white">All arrangements and finalisation of facilities, equipment and other logistics and resources are
                    done as per the clients request.
                    We handle all necessary correspondence with delegates quickly and efficiently. Assistance is
                    offered with any pre-and post-conference tours.</p>
                </div>

                <button class="accordion">Managing the Event</button>
                <div class="panel">
                  <p class="abt white">QuinDara Events stage-manage the event, treating it as a theatrical production, with a
                    wonderful atmosphere and utmost courtesy and professionalism at all levels.</p>
                </div>

                <button class="accordion">Financial Control</button>
                <div class="panel">
                  <p class="abt white">We produce budgets and liaise on an ongoing basis with the client to produce a quality event
                    within the designated budget. We organise all events, matching them with suitable function venues as per the client’s
                    requested criteria. We believe that any event should be exciting and full-fill the desired needs of
                    the client.</p>
                </div>

                <button class="accordion">Managing the Event</button>
                <div class="panel">
                  <p class="abt white">Our conference planning makes the bookings, payment, registration and attendance as easy as
                    possible. We strive to improve our performance as professional conference organisers, making
                    administration details easy and intuitive so that guests feel motivated and leave with positive
                    perceptions about the host and the event.<br><br>
                    ‘We will manage your event from concept to completion”</p>
                </div>
                <button class="accordion">Objectives</button>

            <div class="panel">
            <p class="abt white">
                <ul class="listing">
                  <li>To develop a sound marketing strategy with cost effective solutions</li>
                  <li>To generate adequate internal resources to finance the company's investment plans, working capital needs and build its reserves for future growth</li>
                  <li> To continuously enhance the quality of service</li>
                   <li>To seek, promote and foster excellence amongst the team and improve productivity.</li>
                    <li>To allocate resources and to strive to reduce the cost per unit of different services</li>
                    <li> To actively seek expansion of the company's operation</li>
                </ul>
            </p>
            </div>

            <button class="accordion">Quality Assurance </button>
            <div class="panel">
              <p class="abt white">We strive towards achieving the following values:

                • To provide our customers with services and products of the highest quality that meet
                agreed specifications and deadlines through fully understanding their needs and our
                capability to meet them
                • To minimize all forms of loss and wastage through planned and systematic control of all
                our activities, resulting in increased benefits for all stakeholders.
                • Strive to satisfy and delight our clients as the most important stakeholders in our
                business.
                • Creating ongoing winning partnerships, with suppliers being our allies in serving the
                interests of our clients
                • To develop individual skills, self-discipline and a true sense of self – worth of all team
                members. Our success is dependent upon the collective energy and intelligence of our
                team members. We strive to create a work environment where motivated team members
                can flourish and succeed to their highest potential.
                • We appreciate effort and QuinDara Events are based on a concept of reliability, quality
                and value for money. Our values stem from respect and integrity, superior service,
                customer satisfaction, ethics and discipline.
                ‘We will manage your event from concept to completion”</p>

            </div>

            <button class="accordion">QuinDara a Safer Place</button>
            <div class="panel">
              <p class="abt white">QuinDara Events is a owned entity which aims at introducing strategies and practices
                that will ensure the gender and racial profile of the company is balanced and
                representative. We support the LGBTQ Community as well making it a safer place for
                each individual or guests who are a part o QuinDara Events. QuinDara Events will
                support Indian business, and encourage these companies to do the same while being
                cognisant to procure from those compliant with the codes of good practice.</p>
            </div>

            <button class="accordion">Social Responsibility</button>
            <div class="panel">
              <p class="abt white">QuinDara Events acknowledges its responsibility to its clients, the staff and the wider
                community.
                • We will foster healthy relationships with our clients based on honesty and reliability.
                • Caring about our communities and environment is the cornerstone of our value base. The
                company contributes to the growth and development of the communities within which it
                operates by becoming involved in community development projects. We believe that
                education and sport are crucial elements to ensure positive social change.
                • QuinDara employs and trains local people. We do not discriminate.
                • The company conducts its business in an environmentally sustainable manner.
                • We support the social, emotional and physical well-being of employees by providing a
                healthy, stimulating work environment.
                “ The most successful event is the one, that
                achieves your goals and exceeds your
                expectations” – Team QuinDara</p>
            </div>

            </div>
        </div>
    </section>
<!-- end of services -->


<!-- Add Services -->
    <section class="album-section section-padding">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </section>
<!-- end of services -->

<!--Event gallery -->
    <section class="gallery-section black-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-heading no-padding">
                        <h2>What <strong>We Do</strong></h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="gallery-filter">
                        <ul>
                            <li><a href="#!" data-filter="*">ALL</a></li>
                            <li><a href="#!" data-filter=".concert">Seminar</a></li>
                            <li><a href="#!" data-filter=".poster">Weddings</a></li>
                            <li><a href="#!" data-filter=".painting">Concerts</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="gallery-grid">
                <div class="grid-item painting poster">
                    <img src="images/event/event1.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event1.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item grid-another-width concert">
                    <img src="images/event/event2.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event2.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item concert">
                    <img src="images/event/event3.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event3.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item painting concert">
                    <img src="images/event/event4.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event4.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>

                <div class="grid-item poster concert">
                    <img src="images/event/event5.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event5.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item  painting poster">
                    <img src="images/event/event6.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event6.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
                <div class="grid-item poster">
                    <img src="images/event/event7.jpg" class="img-responsive" alt="portfolio image" />
                    <div class="base-gradient-bg overlay"></div>
                    <a href="images/event/event7.jpg" class="zoom-button"><i class="ion-ios-plus-empty"></i></a>
                </div>
            </div>
        </div>
    </section>
    <!-- end of gallery -->
<!-- <div class="main-content pdt40  event-list-content">
    <div class="container">
        <div class="section-heading">
            <h2>Event <strong>list</strong></h2>
        </div>
        <div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">26 March, 17</div>
                    <div class="singer">Rock festival</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-one" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">10 April, 17</div>
                    <div class="singer">Metal mania</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-two" class="musica-counter-active" data-value-year="2017" data-value-month="4" data-value-day="10" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">26 March, 17</div>
                    <div class="singer">Rock festival</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-three" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">26 March, 17</div>
                    <div class="singer">Rock festival</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-four" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">10 April, 17</div>
                    <div class="singer">Metal mania</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-five" class="musica-counter-active" data-value-year="2017" data-value-month="4" data-value-day="10" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">26 March, 17</div>
                    <div class="singer">Rock festival</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-six" class="musica-counter-active" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
            <div class="upcoming-event">
                <div class="fornt-part">
                    <div class="date">10 April, 17</div>
                    <div class="singer">Metal mania</div>
                    <div class="venue">Melbourne, Australia</div>
                    <div class="detail text-right"><a href="#" class="musica-button-two">details</a></div>
                </div>
                <div class="back-part">
                    <div class="event-counter">
                        <div id="event-counter-seven" class="musica-counter-active" data-value-year="2017" data-value-month="4" data-value-day="10" data-value-zone="+10" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<script type="text/javascript">
    var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
@endsection
