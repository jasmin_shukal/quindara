@extends('layouts.app')

@section('content')

<section class="banner-area event-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1><strong>QuinDara Communication</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->
       
        <div class="main-content pdt40  event-detail-content">
            <div class="container">
                <div class="event-detail-text">
                    <div class="section-heading">
                        <h2>QuinDara  <strong>Communication</strong></h2>
                    </div>
                    <p class="abt">
                        QuinDara communications is a new age digital content company that brings together content, technology and data delivery with high quality creatives and sales solutions.
                        As a digital agency, pioneering performance and paid marketing. We assist in boosting client’s business online. 
                        We design content to help create the awareness about your business/company/product/services.
                        and Our strategise to reach the right traffic and filter the right audience to help you get leads and deliver by capturing the conversations. QuinDara is a one stop solution for you company to get noticed by a wide digital audience and make a mark as a brand.
                    </p>
                 </div>

            </div>
        </div>

    <!-- mission aand Vision -->
    <section class="text-center core">
      <h2>Focusing on</h2><br><br>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200" >
            <span class="fa fa-bullhorn"></span>
            <h3 class="icons">Awareness</h3>
          </div>
          <div class="col-lg-3 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200">
            <span class="fa fa-users"></span>
            <h3 class="icons">Traffic </h3>
          </div>
          <div class="col-lg-3 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200">
            <span class="fa fa-list-alt"></span>
            <h3 class="icons">Leads </h3>
          </div>
          <div class="clearfix visible-md-block visible-sm-block"></div>
          <div class="col-lg-3 col-sm-6 col-ex-12 core-item wow lightSpeedIn" data-wow-offset="200">
            <span class="fa fa-comments-o"></span>
            <h3 class="icons"> Conversions</h3>
          </div>
        </div>
      </div>
    </section>
    <!-- end of mission - vision -->

    <div class="section section-padding artist-section">
            <div class="container">
                 <div class="section-heading text-center">
               <h1>What <strong>We Do</strong></h1>
            </div>
                <div class="row artists">
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/social-media.jpg')}}" alt="seo">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html">Social media marketing/Seo </a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/lead.jpg')}}" alt="Lead Generation">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html">Lead generation</a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/brand.jpg')}}" alt="Brand-managemen">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html" >Brand management</a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/corporate-profiling.jpg')}}" alt="Corporate profiling">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html">Corporate profiling</a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/seminar.jpg')}}" alt="Seminar">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html">seminar / webinar’s</a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                        <div class="artist">
                            <a class="artist-avatar" href="artist-single.html">
                                    <img src="{{asset('/images/services/live-event.jpg')}}" alt="event">
                                </a>
                            <h4 class="artist-name bhavu"><a href="artist-single.html">Live Events</a></h4>
                           <!--  <p class="artist-role">Rock Singer</p> -->
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <!--lisy  -->
    <div class="main-content pdt40  event-list-content">
        <div class="container">
            <div class="section-heading">
                <h2>Our <strong>Education</strong> Partner </h2>
            </div>
            <div>
                <div class="upcoming-event">
                    <div class="fornt-part">
                        <div class="date">26 March, 17</div>
                        <div class="singer">BathSpa University </div>
                        <div class="venue">UK</div>
                        <div class="detail text-right"><a href="{{url('/bathspa')}}" class="musica-button-two">Details</a></div>
                    </div>
                   <!--  <div class="back-part">
                        <div class="event-counter">
                            <div id="event-counter-one" class="musica-counter-active is-countdown" data-value-year="2017" data-value-month="5" data-value-day="28" data-value-zone="+10"><span class="countdown-row countdown-show3"><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Hours</span></span><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Min</span></span><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Sec</span></span></span></div>
                        </div>
                    </div> -->
                </div><!--/.upcoming-event-->
                <div class="upcoming-event">
                    <div class="fornt-part">
                        <div class="date">10 April, 17</div>
                        <div class="singer">OEC University</div>
                        <div class="venue">India & Dubai</div>
                        <div class="detail text-right"><a href="{{url('/oec')}}" class="musica-button-two">details</a></div>
                    </div>
                    <!-- <div class="back-part">
                        <div class="event-counter">
                            <div id="event-counter-two" class="musica-counter-active is-countdown" data-value-year="2017" data-value-month="4" data-value-day="10" data-value-zone="+10"><span class="countdown-row countdown-show3"><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Hours</span></span><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Min</span></span><span class="countdown-section"><span class="countdown-amount">00</span><span class="countdown-period">Sec</span></span></span></div>
                        </div>
                    </div> -->
                </div><!--/.upcoming-event-->
            </div>
        </div>
    </div>
@endsection
