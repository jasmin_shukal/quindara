@extends('layouts.app')
@section('content')
<section class="banner-area album-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-title">
                    <h1>Our <strong>Team</strong></h1>
                </div>
            </div>
        </div>
    </div>
</section><!--/.banner-area-->
<div class="main-content pdt40 pdb90">
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album1.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album2.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album3.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album1.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album2.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="album-item">
                    <a href="#">
                        <img src="images/album/album3.jpg" class="img-responsive" alt="album">
                        <div class="overlay base-gradient-bg"></div>
                        <h3 class="heading">Sound of Silence</h3>
                        <span class="sub-heading">instrumemntal</span>
                    </a>
                </div><!--/.album-item-->
            </div>
        </div>
    </div>
</div>
@endsection
