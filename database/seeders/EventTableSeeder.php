<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\Team;
use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artist=array(
            "Alka Yagnik",
"Anuradha Paudwal",
"Asha Bhosle",
"S. D. Burman",
"Kalyanji-Anandji",
"Chorus",
"Suresh Wadkar",
"Mukesh",
"Hari Om Sharan",
"Manna Dey",
"Various Artist",
"Traditional",
"Mahendra Kapoor",
"Manhar Udhas",
"Usha Mangeshkar",
"Nandu Honap",
"Tushar Joshi",
"Geeta Dutt",
"Hemlata",
"Sanjivani",
"Suman Kalyanpur",
"C. Ramchandra",
"M.S. Subbulakshmi",
"Shrinivas Khale",
"Sudhir Phadke",
"Pujya Bhaishri Rameshbhai Oza",
"Anupama Deshapande",
"Talat Aziz",
"Radha Vishwanathan",
"Krishna Kalle",
"Purshotam Upadhyay",
"Jay Mehta");
        $faker = \Faker\Factory::create();
        // Event::create([
        //     'name' => 'Admin',
        //     'email' => 'admin@admin.com',
        //     'password' => bcrypt('123456'),
        // ]);
        for ($i = 0; $i < 10; $i++) {
            Event::create([
                'name' => $faker->sentence,
                'description' => $faker->paragraph,
                'date' => $faker->dateTimeBetween('now', '+3 years'),
                'venue' => $faker->randomElement(["vadodara", "Rajkot", "Jamnagar","ahmedabad"]),
                'artist' => $faker->randomElement($artist),
                'lat' => $faker->latitude(),
                'long' => $faker->longitude()
            ]);
        }

        for ($i=0; $i < 3; $i++) {
            Team::create([
                'name' => $faker->name,
                'postion' => $faker->sentence,
                'desctiption' => $faker->paragraph
            ]);
        }
    }
}
