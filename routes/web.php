<?php

use App\Http\Controllers\FrontController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class,'index']);

Route::get('/event', function () {
    return view('event');
});

Route::get('/oec',function(){
    return view('oec');
});

Route::get('/bathspa',function(){
    return view('bathspa');
});


Route::get('/event-detail', function () {
    return view('event_detail');
});


Route::get('/wcu', function () {
    return view('wcu');
});


Route::get('/gallery', function () {
    return view('gallery');
});

Route::get('/contact', function () {
    return view('contact');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
