<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\HeaderSlider;
use App\Models\Service;
use Illuminate\Http\Request;


class FrontController extends Controller
{
    public function index(Request $request)
    {
        $all_header_slider = HeaderSlider::all();
        $all_service =Service::all();
        return view('welcome',compact('all_service','all_header_slider'));
    }
}
