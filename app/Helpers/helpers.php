<?php

use Illuminate\Support\Facades\Auth;
use App\Models\MediaUpload;
use App\Models\StaticOption;


function get_attachment_image_by_id($id, $size = null, $default = false)
{
    $image_details = MediaUpload::find($id);
    $return_val = [];
    $image_url = config('app.file_storage_main_path');

    switch ($size) {
        case "large":
            if (file_exists('assets/uploads/media-uploader/large-' . $image_details->path)) {
                $image_url = asset('assets/uploads/media-uploader/large-' . $image_details->path);
            }
            break;
        case "grid":
            if (file_exists('assets/uploads/media-uploader/grid-' . $image_details->path)) {
                $image_url = asset('assets/uploads/media-uploader/grid-' . $image_details->path);
            }
            break;
        case "thumb":
            if (file_exists('assets/uploads/media-uploader/thumb-' . $image_details->path)) {
                $image_url = asset('assets/uploads/media-uploader/thumb-' . $image_details->path);
            }
            break;
        default:
                $image_url = $image_url.$image_details->path;
            break;
    }
    if (!empty($image_details)){
        $return_val['image_id'] = $image_details->id;
        $return_val['path'] = $image_details->path;
        $return_val['img_url'] = $image_url;
    }elseif (empty($image_details) && $default){
        $return_val['img_url'] = asset('assets/uploads/no-image.png');
    }

    return $return_val;
}


function get_static_option($key)
{
    if (StaticOption::where('option_name', $key)->first()) {
        $return_val = StaticOption::where('option_name', $key)->first();
        return $return_val->option_value;
    }
    return null;
}
